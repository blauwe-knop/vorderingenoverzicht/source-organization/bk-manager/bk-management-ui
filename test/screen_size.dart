class ScreenSize {
  const ScreenSize(this.name, this.width, this.height, this.pixelRatio);
  final String name;
  final double width, height, pixelRatio;
}

const iPhone8 = ScreenSize('iPhone_8', 414, 736, 3);
const iPhone13ProMax = ScreenSize('iPhone_13_Pro_Max', 414, 896, 3);
const desktop = ScreenSize('Desktop', 1920, 1080, 1);
