// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/app.dart';
import 'package:bk_management_ui/clients/bk_management_client_mock.dart';
import 'package:bk_management_ui/providers/key_pair_provider.dart';
import 'package:bk_management_ui/providers/menu_provider.dart';
import 'package:bk_management_ui/providers/organization_settings_provider.dart';
import 'package:bk_management_ui/providers/scheme_state_provider.dart';
import 'package:bk_management_ui/screens/main/main_screen.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';

import 'screen_size.dart';
import 'screen_size_manager.dart';

void main() {
  testWidgets('Mainscreen starts on startup', (WidgetTester tester) async {
    await tester.setScreenSize(desktop);
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          ChangeNotifierProvider<MenuProvider>(
            create: (context) => MenuProvider(),
          ),
          ChangeNotifierProvider<OrganizationSettingsProvider>(
            create: (context) => OrganizationSettingsProvider(
              BkManagementClientMock(),
            ),
          ),
          ChangeNotifierProvider<KeyPairProvider>(
            create: (context) => KeyPairProvider(
              BkManagementClientMock(),
            ),
          ),
          ChangeNotifierProvider<SchemeStateProvider>(
            create: (context) => SchemeStateProvider(
              BkManagementClientMock(),
            ),
          ),
        ],
        child: const BkManagerApp(),
      ),
    );

    final mainScreenFinder = find.byType(MainScreen);

    expect(mainScreenFinder, findsOneWidget);
  });
}
