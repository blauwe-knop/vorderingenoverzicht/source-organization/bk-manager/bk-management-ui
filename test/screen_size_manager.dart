import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';

import 'screen_size.dart';

extension ScreenSizeManager on WidgetTester {
  Future<void> setScreenSize(ScreenSize screenSize) async {
    return _setScreenSize(
      width: screenSize.width,
      height: screenSize.height,
      pixelRatio: screenSize.pixelRatio, //pixelDensity
    );
  }

  Future<void> _setScreenSize({
    required double width,
    required double height,
    required double pixelRatio,
  }) async {
    final size = Size(width, height);
    await binding.setSurfaceSize(size);
    view.physicalSize = size;
    view.devicePixelRatio = pixelRatio;
  }
}
