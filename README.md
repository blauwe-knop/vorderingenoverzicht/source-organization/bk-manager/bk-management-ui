# bk-management-ui

The Blauwe Knop Management UI to register at the Scheme.

### Launch Screen generation

To generate the Launch Screens, change settings in pubspec.yaml and run

```sh
dart run flutter_native_splash:create
```