// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:bk_management_ui/screens/home/home_screen.dart';
import 'package:bk_management_ui/providers/organization_settings_provider.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final organizationSettings =
        Provider.of<OrganizationSettingsProvider>(context).organizationSettings;
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Column(
              children: [
                (organizationSettings?.logoUrl != null &&
                        organizationSettings?.logoUrl != "")
                    ? SvgPicture.network(
                        organizationSettings?.logoUrl ?? "",
                        width: 100,
                        height: 100,
                        semanticsLabel: 'Organization Logo',
                      )
                    : SvgPicture.asset(
                        "assets/images/bk_logo.svg",
                        width: 100,
                        height: 100,
                        semanticsLabel: 'Organization Logo',
                      ),
                const Spacer(),
                Text(
                  organizationSettings?.name ?? "",
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          DrawerListTile(
            title: "Bronorganisatiebeheer",
            press: () => Navigator.of(context)
                .pushReplacementNamed(HomeScreen.routePath),
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    super.key,
    required this.title,
    required this.press,
  });

  final String title;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      horizontalTitleGap: 0.0,
      title: Text(
        title,
      ),
    );
  }
}
