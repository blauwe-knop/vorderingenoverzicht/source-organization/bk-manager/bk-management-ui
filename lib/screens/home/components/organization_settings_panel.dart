// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/models/scheme_state.dart';
import 'package:bk_management_ui/providers/organization_settings_provider.dart';
import 'package:bk_management_ui/providers/scheme_state_provider.dart';
import 'package:bk_management_ui/screens/home/components/copy_button.dart';
import 'package:bk_management_ui/screens/home/components/organization_details_panel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:bk_management_ui/constants.dart';
import 'package:bk_management_ui/providers/key_pair_provider.dart';
import 'package:bk_management_ui/models/organization_settings.dart';
import 'package:bk_management_ui/theme.dart';

class OrganizationSettingsPanel extends StatelessWidget {
  const OrganizationSettingsPanel({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final OrganizationSettings? organizationSettings =
        Provider.of<OrganizationSettingsProvider>(context).organizationSettings;
    final String? publicKey = Provider.of<KeyPairProvider>(context).publicKey;
    final SchemeState? schemeState =
        Provider.of<SchemeStateProvider>(context).schemeState;
    final isKeyPairGenerated = (publicKey != null) && publicKey.isNotEmpty;

    return Expanded(
      child: Container(
        padding: const EdgeInsets.all(defaultPadding),
        decoration: BoxDecoration(
          color: UserTheme.blauweknop()?.secondaryHeaderColor,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            OrganizationDetailsPanel(
                name: organizationSettings?.name ?? "",
                oin: organizationSettings?.oin ?? "",
                discoveryUrl:
                    organizationSettings?.discoveryUrl.toString() ?? "",
                logoUrl: organizationSettings?.logoUrl ?? ""),
            const SizedBox(height: 10),
            const Divider(),
            const SizedBox(height: 10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Document type: ",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                Text(
                  "Vorderingenoverzicht Rijk",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ],
            ),
            const SizedBox(height: 20),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Keypair status: ",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                if (isKeyPairGenerated)
                  Text(
                    "gegenereerd ",
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                          color: Colors.green,
                        ),
                  ),
                if (!isKeyPairGenerated)
                  Text(
                    "niet gegenereerd ",
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(color: Colors.red[600]),
                  ),
                if (!isKeyPairGenerated)
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        backgroundColor: Colors.green[600],
                        foregroundColor: Colors.white,
                        textStyle: const TextStyle(color: Colors.white)),
                    onPressed: () {
                      Provider.of<KeyPairProvider>(context, listen: false)
                          .generate()
                          .whenComplete(() {
                        const snackBar = SnackBar(
                          content: Text('Keypair gegenereerd.'),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      });
                    },
                    child: const Text("Genereer"),
                  ),
              ],
            ),
            const SizedBox(height: 10),
            const Divider(),
            const SizedBox(height: 10),
            Wrap(
              children: [
                Text(
                  "Organisatiestatus in het stelsel: ",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                if ((schemeState != null) && schemeState.registered)
                  Text(
                    "aangemeld ",
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                          color: Colors.green,
                        ),
                  ),
                if ((schemeState != null) && !schemeState.registered)
                  Text(
                    "nog niet aangemeld ",
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(color: Colors.red[600]),
                  ),
                if ((schemeState != null) && !schemeState.registered)
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        backgroundColor: Colors.green[600],
                        foregroundColor: Colors.white,
                        textStyle: const TextStyle(color: Colors.white)),
                    onPressed: () {
                      Provider.of<SchemeStateProvider>(context, listen: false)
                          .register()
                          .whenComplete(() {
                        const snackBar = SnackBar(
                          content: Text('Aangemeld bij stelsel beheer.'),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      });
                    },
                    child: const Text("Aanmelden"),
                  ),
              ],
            ),
            const SizedBox(height: 20),
            Wrap(
              children: [
                Text(
                  "Goedkeuring door stelselbeheerder: ",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                if ((schemeState != null) && schemeState.approved)
                  Text(
                    "goedgekeurd ",
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                          color: Colors.green,
                        ),
                  ),
                if ((schemeState != null) && !schemeState.approved)
                  Text(
                    "nog niet goedgekeurd ",
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(color: Colors.red[600]),
                  ),
                if ((schemeState != null) && !schemeState.approved)
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        backgroundColor: Colors.green[600],
                        foregroundColor: Colors.white,
                        textStyle: const TextStyle(color: Colors.white)),
                    onPressed: () {
                      Provider.of<SchemeStateProvider>(context, listen: false)
                          .refresh();
                    },
                    child: const Text("Controleren"),
                  ),
              ],
            ),
            if (isKeyPairGenerated) ...[
              const SizedBox(height: 10),
              const Divider(),
              const SizedBox(height: 10),
              Text(
                "Public key: ",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(height: 10),
              Row(
                children: [
                  Container(
                    padding: const EdgeInsets.all(defaultPadding),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Text(
                      publicKey,
                    ),
                  ),
                  const SizedBox(width: 10),
                  CopyButton("Public key", publicKey),
                ],
              )
            ]
          ],
        ),
      ),
    );
  }
}
