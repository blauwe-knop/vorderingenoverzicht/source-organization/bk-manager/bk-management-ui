// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/screens/home/components/copy_button.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:bk_management_ui/constants.dart';
import 'package:flutter/material.dart';

class OrganizationDetailsPanel extends StatelessWidget {
  final String name;
  final String oin;
  final String discoveryUrl;
  final String? logoUrl;

  const OrganizationDetailsPanel({
    required this.name,
    required this.oin,
    required this.discoveryUrl,
    this.logoUrl,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              (logoUrl != null && logoUrl != "")
                  ? SvgPicture.network(
                      width: 48,
                      height: 48,
                      logoUrl!,
                      semanticsLabel: 'Organization Logo')
                  : SvgPicture.asset("assets/images/bk_logo.svg",
                      width: 48,
                      height: 48,
                      semanticsLabel: 'Organization Logo'),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    Row(
                      children: [
                        Text(
                          "OIN: $oin",
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                        const SizedBox(width: 10),
                        CopyButton("OIN", oin),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          "Discovery url: $discoveryUrl",
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                        const SizedBox(width: 10),
                        CopyButton("Discovery url", discoveryUrl),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
