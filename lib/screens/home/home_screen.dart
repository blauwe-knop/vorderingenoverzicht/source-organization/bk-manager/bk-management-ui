// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/constants.dart';
import 'package:bk_management_ui/screens/home/components/header.dart';
import 'package:bk_management_ui/screens/home/components/organization_settings_panel.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});
  static const routePath = '/';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.all(defaultPadding),
        child: const Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Header(),
            SizedBox(height: defaultPadding),
            OrganizationSettingsPanel(),
          ],
        ),
      ),
    );
  }
}
