// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/clients/bk_management_client_live.dart';
import 'package:bk_management_ui/providers/key_pair_provider.dart';
import 'package:bk_management_ui/providers/menu_provider.dart';
import 'package:bk_management_ui/providers/organization_settings_provider.dart';
import 'package:bk_management_ui/providers/scheme_state_provider.dart';
import 'package:bk_management_ui/providers/env_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        Provider<EnvProvider>(create: (_) => EnvProvider()),
        ChangeNotifierProvider<MenuProvider>(
          create: (_) => MenuProvider(),
        ),
        ChangeNotifierProvider<OrganizationSettingsProvider>(
          create: (context) {
            final organizationSettingsProvider = OrganizationSettingsProvider(
                BkManagementClientLive(context.read<EnvProvider>()));
            organizationSettingsProvider.get();
            return organizationSettingsProvider;
          },
        ),
        ChangeNotifierProvider<KeyPairProvider>(
          create: (context) {
            final keyPairProvider = KeyPairProvider(
              BkManagementClientLive(context.read<EnvProvider>()),
            );
            keyPairProvider.getPublicKey();
            return keyPairProvider;
          },
        ),
        ChangeNotifierProvider<SchemeStateProvider>(
          create: (context) {
            final schemeStateProvider = SchemeStateProvider(
              BkManagementClientLive(context.read<EnvProvider>()),
            );
            schemeStateProvider.get();
            return schemeStateProvider;
          },
        ),
      ],
      child: const BkManagerApp(),
    ),
  );
}
