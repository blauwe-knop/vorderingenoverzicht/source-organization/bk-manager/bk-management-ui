// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class EnvProvider {
  bool _isLoaded = false;
  String? _baseUrl;

  Future<String> baseUrl() async {
    await loadEnvJson();
    return _baseUrl!;
  }

  Future<void> loadEnvJson() async {
    if (!_isLoaded) {
      var response = await rootBundle.loadString('assets/env.json');
      var responseJson = json.decode(response);

      _baseUrl = responseJson["baseUrl"];

      _isLoaded = true;
    }
  }
}
