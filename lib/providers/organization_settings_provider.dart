// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/clients/bk_management_client.dart';
import 'package:bk_management_ui/models/organization_settings.dart';
import 'package:flutter/material.dart';

class OrganizationSettingsProvider with ChangeNotifier {
  OrganizationSettingsProvider(this._bkManagementClient);

  final BkManagementClient _bkManagementClient;

  OrganizationSettings? organizationSettings;

  void get() {
    var data = _bkManagementClient.getOrganizationSettings();

    data.listen((event) {
      organizationSettings = event;

      notifyListeners();
    });
  }
}
