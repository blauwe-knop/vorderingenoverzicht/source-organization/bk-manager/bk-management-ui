// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/clients/bk_management_client.dart';
import 'package:bk_management_ui/models/scheme_state.dart';
import 'package:flutter/material.dart';

class SchemeStateProvider with ChangeNotifier {
  SchemeStateProvider(this._bkManagementClient);

  final BkManagementClient _bkManagementClient;

  SchemeState? schemeState;

  void get() {
    var data = _bkManagementClient.getSubmitSchemeState();

    data.listen((event) {
      schemeState = event;

      notifyListeners();
    });
  }

  Future<void> register() async {
    await _bkManagementClient.register();
    get();
  }

  Future<void> refresh() async {
    get();
  }
}
