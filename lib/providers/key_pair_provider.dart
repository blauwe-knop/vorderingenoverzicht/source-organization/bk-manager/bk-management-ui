// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';

import 'package:bk_management_ui/clients/bk_management_client.dart';
import 'package:flutter/material.dart';

class KeyPairProvider with ChangeNotifier {
  final BkManagementClient _bkManagementClient;

  KeyPairProvider(this._bkManagementClient);

  String? publicKey;

  void getPublicKey() {
    var data = _bkManagementClient.getPublicKey();

    data.listen((event) {
      publicKey = event;

      notifyListeners();
    });
  }

  Future<void> generate() async {
    await _bkManagementClient.generateKeyPair();
    getPublicKey();
  }
}
