// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/models/logging/device_event_class_id.dart';
import 'package:bk_management_ui/models/organization_settings.dart';
import 'package:bk_management_ui/models/scheme_state.dart';
import 'package:bk_management_ui/providers/env_provider.dart';
import 'package:bk_management_ui/helpers/logger.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'bk_management_client.dart';

class BkManagementClientLive implements BkManagementClient {
  final EnvProvider env;

  BkManagementClientLive(this.env);

  @override
  Stream<OrganizationSettings> getOrganizationSettings() async* {
    var endpoint = await env.baseUrl();
    final response =
        await http.get(Uri.parse('$endpoint/fetch_organization_settings'));

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);

      var result = OrganizationSettings.fromJson(responseJson);

      yield result;
    } else {
      Logger.addApiLog(
          type: DeviceEventClassId.bmu_1,
          message: "Failed to get organization settings",
          action: "get organization settings",
          protocol: "BkManagementClientLive",
          requestUri: '${env.baseUrl()}/getOrganizationSettings/',
          requestMethod: "GET");

      throw Exception('Failed to get organizationsettings');
    }
  }

  @override
  Stream<String?> getPublicKey() async* {
    var endpoint = await env.baseUrl();
    final response = await http.get(Uri.parse('$endpoint/fetch_public_key'));

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);

      String result = responseJson;

      yield result;
    } else if (response.statusCode == 404) {
      yield null;
    } else {
      Logger.addApiLog(
          type: DeviceEventClassId.bmu_2,
          message: "Failed to get public key",
          action: "get public key",
          protocol: "BkManagementClientLive",
          requestUri: '${env.baseUrl()}/getPublicKey/',
          requestMethod: "GET");
      throw Exception('Failed to get public key');
    }
  }

  @override
  Future<void> generateKeyPair() async {
    var endpoint = await env.baseUrl();
    final response = await http.post(Uri.parse('$endpoint/generate_key_pair'));

    if (response.statusCode != 200) {
      Logger.addApiLog(
          type: DeviceEventClassId.bmu_3,
          message: "Failed to generate key pair",
          action: "generate key pair",
          protocol: "BkManagementClientLive",
          requestUri: '${env.baseUrl()}/generateKeyPair/',
          requestMethod: "GET");
      throw Exception('Failed to generate key pair');
    }
  }

  @override
  Stream<SchemeState> getSubmitSchemeState() async* {
    var endpoint = await env.baseUrl();
    final response =
        await http.get(Uri.parse('$endpoint/fetch_registration_state'));

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);

      var result = SchemeState.fromJson(responseJson);

      yield result;
    } else {
      Logger.addApiLog(
          type: DeviceEventClassId.bmu_4,
          message: "Failed to get scheme state",
          action: "getSubmitSchemeState",
          protocol: "BkManagementClientLive",
          requestUri: '${env.baseUrl()}/getSubmitSchemeState/',
          requestMethod: "GET");
      throw Exception('Failed to get scheme state');
    }
  }

  @override
  Future<void> register() async {
    var endpoint = await env.baseUrl();
    final response = await http.post(Uri.parse('$endpoint/register'));

    if (response.statusCode != 200) {
      Logger.addApiLog(
          type: DeviceEventClassId.bmu_5,
          message: "Failed to register to scheme",
          action: "register",
          protocol: "BkManagementClientLive",
          requestUri: '${env.baseUrl()}/register/',
          requestMethod: "GET");
      throw Exception('Failed to register to scheme');
    }
  }
}
