// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/models/organization_settings.dart';
import 'package:bk_management_ui/models/scheme_state.dart';

import 'bk_management_client.dart';

class BkManagementClientMock implements BkManagementClient {
  String? publicKey;
  var schemeState = SchemeState(registered: false, approved: false);
  var isFirstTimeCalledAfterRegistration = false;

  @override
  Stream<OrganizationSettings> getOrganizationSettings() async* {
    yield OrganizationSettings(
      oin: "0123456789",
      name: "Demo organization",
      discoveryUrl: Uri.parse("http://demo-organization.bk"),
      logoUrl: null,
    );
  }

  @override
  Future<void> generateKeyPair() async {
    publicKey = """-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEyByG91NiTZFn8iDnDhaabKDaf/G0
bBblA8ASDgnqF3wI4ZmGvYKbwIS3mMbcN+rUZJpt4wk/9DrVdZVvcAtdAQ==
-----END PUBLIC KEY-----""";
  }

  @override
  Stream<String?> getPublicKey() async* {
    yield publicKey;
  }

  @override
  Stream<SchemeState> getSubmitSchemeState() async* {
    if (isFirstTimeCalledAfterRegistration) {
      schemeState.approved = true;
    }

    isFirstTimeCalledAfterRegistration = schemeState.registered;

    yield schemeState;
  }

  @override
  Future<void> register() async {
    if (!schemeState.registered) {
      schemeState.registered = true;
    }
  }
}
