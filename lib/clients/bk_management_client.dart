// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/models/organization_settings.dart';
import 'package:bk_management_ui/models/scheme_state.dart';

abstract class BkManagementClient {
  Stream<OrganizationSettings> getOrganizationSettings();
  Stream<String?> getPublicKey();
  Stream<SchemeState> getSubmitSchemeState();
  Future<void> register();
  Future<void> generateKeyPair();
}
