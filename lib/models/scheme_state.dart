// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class SchemeState {
  bool registered;
  bool approved;

  SchemeState({
    required this.registered,
    required this.approved,
  });

  factory SchemeState.fromJson(Map<String, dynamic> json) {
    return SchemeState(
      registered: json["registered"],
      approved: json["approved"],
    );
  }
}
