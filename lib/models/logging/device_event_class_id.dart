import 'package:bk_management_ui/models/logging/event_severity_level.dart';

enum DeviceEventClassId {
  bmu_1('FailedToGetOrganizationsSettings', EventSeverityLevel.medium),
  bmu_2('FailedToGetPublicKey', EventSeverityLevel.high),
  bmu_3('FailedToGenerateKeyPair', EventSeverityLevel.high),
  bmu_4('FailedToGetSchemeState', EventSeverityLevel.high),
  bmu_5('FailedToRegister', EventSeverityLevel.high);

  const DeviceEventClassId(this.eventName, this.severityLevel);

  final String eventName;
  final EventSeverityLevel severityLevel;
}
