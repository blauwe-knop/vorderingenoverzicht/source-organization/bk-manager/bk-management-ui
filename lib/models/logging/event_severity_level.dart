enum EventSeverityLevel {
  unknown(0),
  low(2),
  medium(5),
  high(7),
  veryHigh(10);

  const EventSeverityLevel(this.value);

  final int value;
}
