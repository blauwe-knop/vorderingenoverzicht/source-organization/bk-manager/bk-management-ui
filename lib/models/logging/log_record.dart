class LogRecord {
  int? id;
  DateTime timestamp;
  String host;
  int cef;
  String deviceVendor;
  String deviceProduct;
  String deviceVersion;
  String deviceEventClassId;
  String name;
  int severity;
  String flexString1Label;
  String flexString1;
  String flexString2Label;
  String flexString2;
  String act;
  String app;
  String request;
  String requestMethod;

  LogRecord({
    this.id,
    required this.timestamp,
    required this.host,
    required this.cef,
    required this.deviceVendor,
    required this.deviceProduct,
    required this.deviceVersion,
    required this.deviceEventClassId,
    required this.name,
    required this.severity,
    required this.flexString1Label,
    required this.flexString1,
    required this.flexString2Label,
    required this.flexString2,
    required this.act,
    required this.app,
    required this.request,
    required this.requestMethod,
  });
}
