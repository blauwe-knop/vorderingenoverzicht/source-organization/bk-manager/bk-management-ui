// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class OrganizationSettings {
  String oin;
  String name;
  Uri discoveryUrl;
  String? logoUrl;

  OrganizationSettings({
    required this.oin,
    required this.name,
    required this.discoveryUrl,
    this.logoUrl,
  });

  factory OrganizationSettings.fromJson(Map<String, dynamic> json) {
    return OrganizationSettings(
      oin: json["oin"],
      name: json["name"],
      discoveryUrl: Uri.parse(json["discoveryUrl"]),
      logoUrl: json["logoUrl"],
    );
  }
}
