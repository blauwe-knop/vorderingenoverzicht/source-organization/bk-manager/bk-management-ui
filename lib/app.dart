// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/screens/main/main_screen.dart';
import 'package:flutter/material.dart';

class BkManagerApp extends StatelessWidget {
  const BkManagerApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Blauwe Knop Bronorganisatiebeheer',
      debugShowCheckedModeBanner: false,
      // theme: UserTheme.blauweknop(),
      home: MainScreen(),
    );
  }
}
