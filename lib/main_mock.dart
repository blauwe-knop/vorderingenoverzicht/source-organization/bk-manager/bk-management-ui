// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:bk_management_ui/clients/bk_management_client_mock.dart';
import 'package:bk_management_ui/providers/key_pair_provider.dart';
import 'package:bk_management_ui/providers/menu_provider.dart';
import 'package:bk_management_ui/providers/organization_settings_provider.dart';
import 'package:bk_management_ui/providers/scheme_state_provider.dart';
import 'package:bk_management_ui/providers/env_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<MenuProvider>(
          create: (context) => MenuProvider(),
        ),
        ChangeNotifierProvider<OrganizationSettingsProvider>(
          create: (context) {
            final organizationSettingsProvider = OrganizationSettingsProvider(
              BkManagementClientMock(),
            );

            organizationSettingsProvider.get();

            return organizationSettingsProvider;
          },
        ),
        ChangeNotifierProvider<KeyPairProvider>(
          create: (context) {
            final keyPairProvider = KeyPairProvider(
              BkManagementClientMock(),
            );
            return keyPairProvider;
          },
        ),
        ChangeNotifierProvider<SchemeStateProvider>(
          create: (context) {
            final schemeStateProvider = SchemeStateProvider(
              BkManagementClientMock(),
            );
            schemeStateProvider.get();
            return schemeStateProvider;
          },
        ),
        Provider<EnvProvider>(
          create: (_) => EnvProvider(),
        ),
      ],
      child: const BkManagerApp(),
    ),
  );
}
